import json


class Config:
    def __init__(self):
        #self.config = {"Main_PID":          {"P": 3.5,
        #                                     "I": 0.0,
        #                                     "D": 6.0,
        #                                     "Saturation": 0.0},
        #               "Motor_PID":         {"P": 3.0,
        #                                     "I": 0.2,
        #                                     "D": 5.0,
        #                                     "Saturation": 1000.0},
        #               "Speeds":            {"Normal": 75.0,
        #                                     "Lost_track": 50.0,
        #                                     "Track_recording": 50.0,
        #                                     "Optimized": 75.0},
        #               "Sensor_thresholds": [170, 170, 170, 170, 170, 170, 170, 170],
        #               "Sensor_weights":    [-16.0, -9.0, -4.0, -2.0, 2.0, 4.0, 9.0, 16.0]}
#
        #with open("default_config.json", "w") as write_file:
        #    json.dump(self.config, write_file)
        self.config = None
        self.load("default_config.json")

    def update_main_pid(self, p, i, d, saturation):
        self.config["Main_PID"]["P"] = p
        self.config["Main_PID"]["I"] = i
        self.config["Main_PID"]["D"] = d
        self.config["Main_PID"]["Saturation"] = saturation

    def update_motor_pid(self, p, i, d, saturation):
        self.config["Motor_PID"]["P"] = p
        self.config["Motor_PID"]["I"] = i
        self.config["Motor_PID"]["D"] = d
        self.config["Motor_PID"]["Saturation"] = saturation

    def update_speeds(self, normal, lost_track, track_recording, optimized):
        self.config["Speeds"]["Normal"] = normal
        self.config["Speeds"]["Lost_track"] = lost_track
        self.config["Speeds"]["Track_recording"] = track_recording
        self.config["Speeds"]["Optimized"] = optimized

    def update_sensor_thresholds(self, thresholds):
        self.config["Sensor_thresholds"] = thresholds

    def update_sensor_weights(self, weights):
        self.config["Sensor_weights"] = weights

    def load(self, path):
        with open(path, "r") as read_file:
            self.config = json.load(read_file)

    def save(self, path):
        with open(path, "w") as write_file:
            json.dump(self.config, write_file)

