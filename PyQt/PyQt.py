import sys, os
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QFile, QUrl, QObject, Slot, Property, Signal, QStringListModel, QRunnable, QThreadPool
from PySide2.QtQml import QQmlApplicationEngine
import random, time
import serial
from config import Config
import req_msgs




COM_ports = ['COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6',]


class ComConnection:
    def __init__(self, portCOM='COM1'):
        self.portCOM = portCOM
        self.port_handle = serial.Serial()
        self.status = 'disconnected'

    def open_port(self):
        self.status = 'connecting'
        try:
            self.port_handle = serial.Serial(port=self.portCOM,
                                             baudrate=9600,
                                             parity=serial.PARITY_NONE,
                                             stopbits=serial.STOPBITS_ONE,
                                             bytesize=serial.EIGHTBITS,
                                             timeout=1)
        except:
            pass
        if self.port_handle.isOpen():
            self.status = 'connected'
            return True
        else:
            self.status = 'disconnected'
            return False

    def close_port(self):
        if self.port_handle.isOpen():
            self.port_handle.close()
        self.status = 'disconnected'

    def set_port_COM(self, portCOM):
        self.portCOM = portCOM

    def send_req(self, req_msg_id, value=0):
        if req_msg_id == req_msgs.IDLE_REQ:
            packet = bytearray()
            packet.append(req_msgs.IDLE_REQ)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)
            self.port_handle.readline()

        elif req_msg_id == req_msgs.NORMAL_RUN_REQ:
            packet = bytearray()
            packet.append(req_msgs.NORMAL_RUN_REQ)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)
            self.port_handle.readline()

        elif req_msg_id == req_msgs.TRACK_RECORDING_RUN_REQ:
            packet = bytearray()
            packet.append(req_msgs.TRACK_RECORDING_RUN_REQ)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)
            self.port_handle.readline()

        elif req_msg_id == req_msgs.OPTIMIZED_RUN_REQ:
            packet = bytearray()
            packet.append(req_msgs.OPTIMIZED_RUN_REQ)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)
            self.port_handle.readline()

        elif req_msg_id == req_msgs.SET_MAIN_PID_KP_REQ:
            packet = bytearray()
            value = int(value * 10)
            packet.append(req_msgs.SET_MAIN_PID_KP_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MAIN_PID_KI_REQ:
            packet = bytearray()
            value = int(value * 100.0)
            packet.append(req_msgs.SET_MAIN_PID_KI_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MAIN_PID_KD_REQ:
            packet = bytearray()
            value = int(value * 10)
            packet.append(req_msgs.SET_MAIN_PID_KD_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MAIN_PID_MAX_INT_VALUE_REQ:
            packet = bytearray()
            value2 = int(value / 255)
            value = value % 255
            value1 = int(value)
            packet.append(req_msgs.SET_MAIN_PID_MAX_INT_VALUE_REQ)
            packet.append(value1)
            packet.append(value2)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_NORMAL_MODE_SPEED_REQ:
            packet = bytearray()
            value2 = int(value / 255)
            value = value % 255
            value1 = int(value)
            packet.append(req_msgs.SET_NORMAL_MODE_SPEED_REQ)
            packet.append(value1)
            packet.append(value2)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_LOST_TRACK_SPEED_REQ:
            packet = bytearray()
            value2 = int(value / 255)
            value = value % 255
            value1 = int(value)
            packet.append(req_msgs.SET_LOST_TRACK_SPEED_REQ)
            packet.append(value1)
            packet.append(value2)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_TRACK_RECORDING_SPEED_REQ:
            packet = bytearray()
            value2 = int(value / 255)
            value = value % 255
            value1 = int(value)
            packet.append(req_msgs.SET_TRACK_RECORDING_SPEED_REQ)
            packet.append(value1)
            packet.append(value2)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MOTOR_PID_KP_REQ:
            packet = bytearray()
            value = int(value * 10)
            packet.append(req_msgs.SET_MOTOR_PID_KP_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MOTOR_PID_KI_REQ:
            packet = bytearray()
            value = int(value * 100.0)
            packet.append(req_msgs.SET_MOTOR_PID_KI_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MOTOR_PID_KD_REQ:
            packet = bytearray()
            value = int(value * 10)
            packet.append(req_msgs.SET_MOTOR_PID_KD_REQ)
            packet.append(value)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_MOTOR_PID_MAX_INT_VALUE_REQ:
            packet = bytearray()
            value2 = int(value / 255)
            value = value % 255
            value1 = int(value)
            packet.append(req_msgs.SET_MOTOR_PID_MAX_INT_VALUE_REQ)
            packet.append(value1)
            packet.append(value2)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            packet.append(0x0)
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_SENSOR_WEIGHTS_REQ:
            packet = bytearray()
            packet.append(req_msgs.SET_SENSOR_WEIGHTS_REQ)
            packet.append(int(abs(value[0] * 10)))
            packet.append(int(abs(value[1] * 10)))
            packet.append(int(abs(value[2] * 10)))
            packet.append(int(abs(value[3] * 10)))
            packet.append(int(abs(value[4] * 10)))
            packet.append(int(abs(value[5] * 10)))
            packet.append(int(abs(value[6] * 10)))
            packet.append(int(abs(value[7] * 10)))
            self.port_handle.write(packet)

        elif req_msg_id == req_msgs.SET_SENSOR_THRESHOLDS_REQ:
            packet = bytearray()
            packet.append(req_msgs.SET_SENSOR_THRESHOLDS_REQ)
            packet.append(int(value[0]))
            packet.append(int(value[1]))
            packet.append(int(value[2]))
            packet.append(int(value[3]))
            packet.append(int(value[4]))
            packet.append(int(value[5]))
            packet.append(int(value[6]))
            packet.append(int(value[7]))
            self.port_handle.write(packet)


class ComConnectorOpenThread(QRunnable):
    def __init__(self, connection, root):
        QRunnable.__init__(self)
        self.connection_handler = connection
        self.root = root

    @Slot()
    def run(self):
        if self.connection_handler.open_port():
            self.root.updateConnectionStatus("Disconnect", 2)
        else:
            self.root.updateConnectionStatus("Connect", 0)


class ConfigUploaderThread(QRunnable):
    def __init__(self, root, serial_port, config):
        QRunnable.__init__(self)
        self.root = root
        self.serial_port = serial_port
        self.config = config

    @Slot()
    def run(self):
        self.root.updateProgressBar(0.0)
        self.serial_port.send_req(req_msgs.SET_MAIN_PID_KP_REQ, self.config.config["Main_PID"]["P"])
        self.root.updateProgressBar(0.1)
        self.serial_port.send_req(req_msgs.SET_MAIN_PID_KI_REQ, self.config.config["Main_PID"]["I"])
        self.root.updateProgressBar(0.2)
        self.serial_port.send_req(req_msgs.SET_MAIN_PID_KD_REQ, self.config.config["Main_PID"]["D"])
        self.root.updateProgressBar(0.3)
        self.serial_port.send_req(req_msgs.SET_MAIN_PID_MAX_INT_VALUE_REQ, self.config.config["Main_PID"]["Saturation"])
        self.root.updateProgressBar(0.4)
        self.serial_port.send_req(req_msgs.SET_NORMAL_MODE_SPEED_REQ, self.config.config["Speeds"]["Normal"])
        self.root.updateProgressBar(0.5)
        self.serial_port.send_req(req_msgs.SET_LOST_TRACK_SPEED_REQ, self.config.config["Speeds"]["Lost_track"])
        self.root.updateProgressBar(0.6)
        self.serial_port.send_req(req_msgs.SET_TRACK_RECORDING_SPEED_REQ, self.config.config["Speeds"]["Track_recording"])
        self.root.updateProgressBar(0.7)
        self.serial_port.send_req(req_msgs.SET_MOTOR_PID_KP_REQ, self.config.config["Motor_PID"]["P"])
        self.root.updateProgressBar(0.8)
        self.serial_port.send_req(req_msgs.SET_MOTOR_PID_KI_REQ, self.config.config["Motor_PID"]["I"])
        self.root.updateProgressBar(0.9)
        self.serial_port.send_req(req_msgs.SET_MOTOR_PID_KD_REQ, self.config.config["Motor_PID"]["D"])
        self.serial_port.send_req(req_msgs.SET_MOTOR_PID_MAX_INT_VALUE_REQ, self.config.config["Motor_PID"]["Saturation"])
        self.serial_port.send_req(req_msgs.SET_SENSOR_WEIGHTS_REQ, self.config.config["Sensor_weights"])
        self.serial_port.send_req(req_msgs.SET_SENSOR_THRESHOLDS_REQ, self.config.config["Sensor_thresholds"])
        self.root.updateProgressBar(1.0)


class ConfigDownloaderThread(QRunnable):
    def __init__(self, root):
        QRunnable.__init__(self)
        self.root = root

    @Slot()
    def run(self):
        self.root.updateProgressBar(0.0)
        time.sleep(0.1)
        self.root.updateProgressBar(0.1)
        time.sleep(0.1)
        self.root.updateProgressBar(0.2)
        time.sleep(0.1)
        self.root.updateProgressBar(0.3)
        time.sleep(0.1)
        self.root.updateProgressBar(0.4)
        time.sleep(0.1)
        self.root.updateProgressBar(0.5)
        time.sleep(0.1)
        self.root.updateProgressBar(0.6)
        time.sleep(0.1)
        self.root.updateProgressBar(0.7)
        time.sleep(0.1)
        self.root.updateProgressBar(0.8)
        time.sleep(0.1)
        self.root.updateProgressBar(0.9)
        time.sleep(0.1)
        self.root.updateProgressBar(1.0)


class Backend(QObject):
    def __init__(self):
        super(Backend, self).__init__()

        self.threadpool = QThreadPool()

        # Connect Button Handling
        self.connection_handler = ComConnection()
        self.root = None

        # Run Timer
        self.timer_started = 0

        # Config
        self.config = Config()

    def set_root(self, root):
        self.root = root

    @Slot()
    def connect_button_clicked(self):
        if self.connection_handler.status == "disconnected":
            self.root.updateConnectionStatus("Connecting...", 1)
            worker = ComConnectorOpenThread(self.connection_handler, self.root)
            self.threadpool.start(worker)
        elif self.connection_handler.status == "connected":
            self.root.updateConnectionStatus("Connect", 0)
            self.connection_handler.close_port()
        else:
            pass

    @Slot(int)
    def set_port_COM(self, port):
        if self.connection_handler.status == 'disconnected':
            self.connection_handler.set_port_COM(COM_ports[port])
        else:
            self.root.updateComComboBox(COM_ports.index(self.connection_handler.portCOM))

    @Slot()
    def updateRunTimer(self):
        timer_time = str(time.time() - self.timer_started).split('.')
        timer_time = timer_time[0] + '.' + timer_time[1][0:1]
        self.root.updateRunTimer(timer_time)

    @Slot()
    def normal_run_button_clicked(self):
        # TODO: send run command
        self.connection_handler.send_req(req_msgs.NORMAL_RUN_REQ)
        self.timer_started = time.time()

    @Slot()
    def track_recording_run_button_clicked(self):
        # TODO: send run command
        self.timer_started = time.time()

    @Slot()
    def optimized_run_button_clicked(self):
        # TODO: send run command
        self.timer_started = time.time()

    @Slot()
    def stop_button_clicked(self):
        self.connection_handler.send_req(req_msgs.IDLE_REQ)

    @Slot(str)
    def load_config(self, path):
        path = path[8:]
        self.config.load(path)
        self.root.loadConfig(self.config.config["Main_PID"]["P"],
                             self.config.config["Main_PID"]["I"],
                             self.config.config["Main_PID"]["D"],
                             self.config.config["Main_PID"]["Saturation"],
                             self.config.config["Motor_PID"]["P"],
                             self.config.config["Motor_PID"]["I"],
                             self.config.config["Motor_PID"]["D"],
                             self.config.config["Motor_PID"]["Saturation"],
                             self.config.config["Speeds"]["Normal"],
                             self.config.config["Speeds"]["Lost_track"],
                             self.config.config["Speeds"]["Track_recording"],
                             self.config.config["Speeds"]["Optimized"],
                             self.config.config["Sensor_thresholds"][0],
                             self.config.config["Sensor_thresholds"][1],
                             self.config.config["Sensor_thresholds"][2],
                             self.config.config["Sensor_thresholds"][3],
                             self.config.config["Sensor_thresholds"][4],
                             self.config.config["Sensor_thresholds"][5],
                             self.config.config["Sensor_thresholds"][6],
                             self.config.config["Sensor_thresholds"][7],
                             self.config.config["Sensor_weights"][0],
                             self.config.config["Sensor_weights"][1],
                             self.config.config["Sensor_weights"][2],
                             self.config.config["Sensor_weights"][3],
                             self.config.config["Sensor_weights"][4],
                             self.config.config["Sensor_weights"][5],
                             self.config.config["Sensor_weights"][6],
                             self.config.config["Sensor_weights"][7])


    @Slot(str)
    def save_config(self, path):
        path = path[8:]
        self.config.save(path)

    @Slot(str, str, str, str)
    def update_main_pid(self, p, i, d, sat):
        self.config.update_main_pid(float(p), float(i), float(d), float(sat))

    @Slot(str, str, str, str)
    def update_motor_pid(self, p, i, d, sat):
        self.config.update_motor_pid(float(p), float(i), float(d), float(sat))

    @Slot(str, str, str, str)
    def update_speeds(self, normal, lost_track, trackRecording, optimized):
        self.config.update_speeds(float(normal), float(lost_track), float(trackRecording), float(optimized))

    @Slot(str, str, str, str, str, str, str, str)
    def update_thresholds(self, thr0, thr1, thr2, thr3, thr4, thr5, thr6, thr7):
        thresholds = [int(thr0), int(thr1), int(thr2), int(thr3), int(thr4), int(thr5), int(thr6), int(thr7)]
        self.config.update_sensor_thresholds(thresholds)

    @Slot(str, str, str, str, str, str, str, str)
    def update_weights(self, weights0, weights1, weights2, weights3, weights4, weights5, weights6, weights7):
        weights = [float(weights0), float(weights1), float(weights2), float(weights3), float(weights4), float(weights5), float(weights6), float(weights7)]
        self.config.update_sensor_weights(weights)

    @Slot()
    def upload_config(self):
        worker = ConfigUploaderThread(self.root, self.connection_handler, self.config)
        self.threadpool.start(worker)

    @Slot()
    def download_config(self):
        worker = ConfigDownloaderThread(self.root)
        self.threadpool.start(worker)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    engine = QQmlApplicationEngine()
    backend = Backend()
    engine.rootContext().setContextProperty("backend", backend)
    engine.load('./UI.qml')
    root = engine.rootObjects()[0]
    backend.set_root(root)


    sys.exit(app.exec_())
