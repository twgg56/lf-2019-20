import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Shapes 1.11

Item {
    id: linefollower
    width: 1200
    height: 800

    GridLayout {
        anchors.fill: parent
        rows: 1
        columns: 4

        TabBar {
            id: tabBar
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: 40
            Layout.preferredWidth: 800

            TabButton {
                text: "Control"
            }

            TabButton {
                text: "Config"
            }

            TabButton {
                text: "Track Recording"
            }
        }

        ComboBox {
            id: comboBox
            font.pointSize: 3
            padding: 5
            spacing: 5
            rightPadding: 20
            leftPadding: 20
            bottomPadding: 5
            topPadding: 5
            Layout.preferredHeight: 40
            Layout.preferredWidth: 142
            Layout.fillHeight: false
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        Button {
            id: button
            text: qsTr("Button")
            Layout.preferredHeight: 40
            Layout.preferredWidth: 140
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        BusyIndicator {
            id: busyIndicator
            spacing: 8
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: 40
            Layout.preferredWidth: 40
        }

        StackLayout {
            id: stackLayout
            width: 1200
            height: 761
            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom
            Layout.fillWidth: true
            Layout.columnSpan: 4
            currentIndex: tabBar.currentIndex

            Item {
                id: element1
                Button {
                    id: button1
                    x: 985
                    y: 38
                    text: qsTr("Button")
                }

                Button {
                    id: button2
                    x: 985
                    y: 105
                    text: qsTr("Button")
                }

                Button {
                    id: button3
                    x: 985
                    y: 168
                    text: qsTr("Button")
                }

                Label {
                    id: label
                    x: 0
                    y: 0
                    width: 183
                    height: 122
                    text: qsTr("Label")
                }
            }

            Item {
                TextField {
                    id: textField2
                    x: 383
                    y: 212
                    text: qsTr("Text Field")
                }

                TextField {
                    id: textField1
                    x: 383
                    y: 147
                    text: qsTr("Text Field")
                }

                TextField {
                    id: textField
                    x: 383
                    y: 81
                    text: qsTr("Text Field")
                }


            }

            Item {
                Button {
                    id: button4
                    x: 0
                    y: 0
                    text: qsTr("Button")
                }
            }

        }
    }










}






























































