import QtQuick 2.11
import QtQuick.Window 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0



Window {
    id: window
    width: 1200
    height: 800
    visible: true
    title: "Linefollower Control Center"

    function updateProgressBar(val) {
        progressBar.value = val
    }

    function updateConnectionStatus(text, status_id) {
        connectButton.text = text
        stackLayout.currentIndex = status_id
    }

    function updateComComboBox(id) {
        comboBox.currentIndex = id
    }

    function updateRunTimer(time) {
        label.text = qsTr(time)
    }

    function loadConfig(mainP, mainI, mainD, mainSat,
                        motorP, motorI, motorD, motorSat,
                        normalSp, lostTrackSp, trackRecSp, optimizedSp,
                        thr0, thr1, thr2, thr3, thr4, thr5, thr6, thr7,
                        w0, w1, w2, w3, w4, w5, w6, w7) {

        mainPidP.text = mainP
        mainPidI.text = mainI
        mainPidD.text = mainD
        mainPidSat.text = mainSat
        motorPidP.text = motorP
        motorPidI.text = motorI
        motorPidD.text = motorD
        motorPidSat.text = motorSat
        normalSpeed.text = normalSp
        lostTrackSpeed.text = lostTrackSp
        trackRecordingSpeed.text = trackRecSp
        optimizedSpeed.text = optimizedSp
        thresh0.text = thr0
        thresh1.text = thr1
        thresh2.text = thr2
        thresh3.text = thr3
        thresh4.text = thr4
        thresh5.text = thr5
        thresh6.text = thr6
        thresh7.text = thr7
        weight0.text = w0
        weight1.text = w1
        weight2.text = w2
        weight3.text = w3
        weight4.text = w4
        weight5.text = w5
        weight6.text = w6
        weight7.text = w7
    }


    Timer {
        id: timer
        interval: 100
        running: false
        repeat: true
        onTriggered: backend.updateRunTimer()
    }


    GridLayout {
        visible: true
        anchors.fill: parent
        rows: 1
        columns: 4


        TabBar {
            id: tabBar
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: 40
            Layout.preferredWidth: 819

            TabButton {
                text: "Control"
            }

            TabButton {
                text: "Config"
            }

            TabButton {
                text: "Track Recording"
            }
        }

        ComboBox {
            id: comboBox
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            model: [ "COM1", "COM2", "COM3", "COM4", "COM5", "COM6" ]
            onCurrentIndexChanged: backend.set_port_COM(comboBox.currentIndex)
        }

        Button {
            id: connectButton
            text: "Connect"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: 40
            Layout.preferredWidth: 140
            onClicked: backend.connect_button_clicked()
        }

        StackLayout {
            id: stackLayout
            Layout.maximumHeight: 40
            Layout.maximumWidth: 40
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.preferredHeight: 40
            Layout.preferredWidth: 40

            Image {
                id: image1
                width: 40
                height: 40
                fillMode: Image.PreserveAspectFit
                source: "red-x-on-network-icon-600x526.png"
            }

            BusyIndicator {
                id: busyIndicator
                width: 40
                height: 40
            }

            Image {
                id: image
                width: 40
                height: 40
                fillMode: Image.PreserveAspectFit
                source: "green-check-icon-4.png"
            }


        }

        StackLayout {
            id: stackLayout1
            width: 100
            height: 100
            currentIndex: tabBar.currentIndex
            Layout.columnSpan: 4

            Item {

                GridLayout {
                    anchors.fill: parent
                    rows: 4
                    columns: 2

                    Label {
                        id: label
                        text: qsTr("0.0")
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        padding: 0
                        font.pointSize: 70
                        rightPadding: 0
                        leftPadding: 0
                        bottomPadding: 0
                        topPadding: 0
                        Layout.rowSpan: 4
                        Layout.preferredHeight: 296
                        Layout.preferredWidth: 695
                    }

                    Button {
                        id: button
                        text: qsTr("Start Normal Run")
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 300
                        onClicked: timer.start(), backend.normal_run_button_clicked()
                    }

                    Button {
                        id: button1
                        text: qsTr("Start Track Recording")
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 300
                        onClicked: timer.start(), backend.track_recording_run_button_clicked()
                    }

                    Button {
                        id: button2
                        text: qsTr("Start Optimized Run")
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 300
                        onClicked: timer.start(), backend.optimized_run_button_clicked()
                    }

                    Button {
                        id: button3
                        text: qsTr("STOP")
                        Layout.preferredHeight: 50
                        Layout.preferredWidth: 300
                        onClicked: timer.stop(), backend.stop_button_clicked()
                    }
                }
            }

            Item {
                visible: true

                FileDialog {
                    id: fileDialogLoadConfig
                    title: "Load config ..."
                    folder: shortcuts.home
                    nameFilters: [ "JSON files (*.json)" ]
                    onAccepted: {
                        backend.load_config(fileDialogLoadConfig.fileUrls)
                        console.log("You chose: " + fileDialogLoadConfig.fileUrls)
                    }
                    onRejected: {
                        console.log("Canceled")
                    }
                    }

                    FileDialog {
                    id: fileDialogSaveConfig
                    title: "Save config as ..."
                    selectExisting: false
                    folder: shortcuts.home
                    nameFilters: [ "JSON files (*.json)" ]
                    onAccepted: {
                        backend.save_config(fileDialogSaveConfig.fileUrls)
                        console.log("You chose: " + fileDialogSaveConfig.fileUrls)
                    }
                    onRejected: {
                        console.log("Canceled")
                    }
                    }


                ColumnLayout {
                    anchors.fill: parent


                    RowLayout {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                        ColumnLayout {
                            Layout.fillWidth: false
                            Layout.preferredHeight: 494
                            Layout.preferredWidth: 861

                            GridLayout {
                                Layout.fillWidth: true
                                Layout.fillHeight: false
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                                Layout.preferredHeight: 195
                                Layout.preferredWidth: 775
                                rows: 5
                                columns: 6

                                Label {
                                    id: label14
                                    text: qsTr("Main PID")
                                    Layout.columnSpan: 2
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 206
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                Label {
                                    id: label15
                                    text: qsTr("Motor PID")
                                    Layout.columnSpan: 2
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 206
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                Label {
                                    id: label16
                                    text: qsTr("Speeds")
                                    Layout.columnSpan: 2
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 213
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                Label {
                                    id: label1
                                    text: qsTr("P")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: mainPidP
                                    text: qsTr("3.5")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_main_pid(mainPidP.text, mainPidI.text, mainPidD.text, mainPidSat.text)
                                }

                                Label {
                                    id: label6
                                    text: qsTr("P")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: motorPidP
                                    text: qsTr("3.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_motor_pid(motorPidP.text, motorPidI.text, motorPidD.text, motorPidSat.text)
                                }

                                Label {
                                    id: label10
                                    text: qsTr("Normal Speed")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: normalSpeed
                                    text: qsTr("75.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_speeds(normalSpeed.text, lostTrackSpeed.text, trackRecordingSpeed.text, optimizedSpeed.text)
                                }

                                Label {
                                    id: label2
                                    text: qsTr("I")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: mainPidI
                                    text: qsTr("0.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_main_pid(mainPidP.text, mainPidI.text, mainPidD.text, mainPidSat.text)
                                }

                                Label {
                                    id: label7
                                    text: qsTr("I")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: motorPidI
                                    text: qsTr("0.2")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_motor_pid(motorPidP.text, motorPidI.text, motorPidD.text, motorPidSat.text)
                                }

                                Label {
                                    id: label11
                                    text: qsTr("Lost Track Speed")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: lostTrackSpeed
                                    text: qsTr("50.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_speeds(normalSpeed.text, lostTrackSpeed.text, trackRecordingSpeed.text, optimizedSpeed.text)
                                }

                                Label {
                                    id: label4
                                    text: qsTr("D")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: mainPidD
                                    text: qsTr("6.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_main_pid(mainPidP.text, mainPidI.text, mainPidD.text, mainPidSat.text)
                                }

                                Label {
                                    id: motorPidDLabel
                                    text: qsTr("D")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: motorPidD
                                    text: qsTr("5.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_motor_pid(motorPidP.text, motorPidI.text, motorPidD.text, motorPidSat.text)
                                }

                                Label {
                                    id: label12
                                    text: qsTr("Track Recording Speed")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: trackRecordingSpeed
                                    text: qsTr("50.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_speeds(normalSpeed.text, lostTrackSpeed.text, trackRecordingSpeed.text, optimizedSpeed.text)
                                }

                                Label {
                                    id: label5
                                    text: qsTr("Integral Saturation")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: mainPidSat
                                    text: qsTr("0.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_main_pid(mainPidP.text, mainPidI.text, mainPidD.text, mainPidSat.text)
                                }

                                Label {
                                    id: label9
                                    text: qsTr("Integral Saturation")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: motorPidSat
                                    text: qsTr("1000.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_motor_pid(motorPidP.text, motorPidI.text, motorPidD.text, motorPidSat.text)
                                }

                                Label {
                                    id: label13
                                    text: qsTr("Optimized Run Speed")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: optimizedSpeed
                                    text: qsTr("75.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 100
                                    onEditingFinished: backend.update_speeds(normalSpeed.text, lostTrackSpeed.text, trackRecordingSpeed.text, optimizedSpeed.text)
                                }
                            }

                            GridLayout {
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                rows: 4
                                columns: 8

                                Label {
                                    id: label17
                                    text: qsTr("Sensor Thresholds")
                                    Layout.columnSpan: 8
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 754
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: thresh0
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh1
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh2
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh3
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh4
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh5
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh6
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                TextField {
                                    id: thresh7
                                    text: qsTr("170")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_thresholds(thresh0.text, thresh1.text, thresh2.text, thresh3.text, thresh4.text, thresh5.text, thresh6.text, thresh7.text)
                                }

                                Label {
                                    id: label18
                                    text: qsTr("Sensor Weights")
                                    Layout.columnSpan: 8
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 754
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                TextField {
                                    id: weight0
                                    text: qsTr("-16.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight1
                                    text: qsTr("-9.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)

                                }

                                TextField {
                                    id: weight2
                                    text: qsTr("-4.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight3
                                    text: qsTr("-2.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight4
                                    text: qsTr("2.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight5
                                    text: qsTr("4.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight6
                                    text: qsTr("9.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }

                                TextField {
                                    id: weight7
                                    text: qsTr("16.0")
                                    Layout.preferredHeight: 35
                                    Layout.preferredWidth: 80
                                    onEditingFinished: backend.update_weights(weight0.text, weight1.text, weight2.text, weight3.text, weight4.text, weight5.text, weight6.text, weight7.text)
                                }
                            }
                        }

                        ColumnLayout {
                            Layout.preferredHeight: 494
                            Layout.preferredWidth: 201

                            ColumnLayout {
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                Label {
                                    id: label20
                                    height: 40
                                    text: qsTr("Load / Save")
                                    Layout.preferredHeight: 40
                                    Layout.preferredWidth: 180
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                Button {
                                    id: button4
                                    text: qsTr("Load from file")
                                    Layout.preferredWidth: 180
                                    onClicked: fileDialogLoadConfig.open()
                                }

                                Button {
                                    id: button5
                                    text: qsTr("Save")
                                    Layout.preferredWidth: 180
                                    onClicked: fileDialogSaveConfig.open()
                                }
                            }

                            ColumnLayout {
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                                Label {
                                    id: label19
                                    text: qsTr("Robot Connection")
                                    Layout.preferredHeight: 44
                                    Layout.preferredWidth: 180
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }

                                Button {
                                    id: button6
                                    text: qsTr("Download running config")
                                    Layout.preferredWidth: 180
                                    onClicked: backend.download_config()
                                }

                                Button {
                                    id: button7
                                    text: qsTr("Upload config")
                                    Layout.preferredWidth: 180
                                    onClicked: backend.upload_config()
                                }
                            }
                        }
                    }

                    ProgressBar {
                        id: progressBar
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.preferredHeight: 61
                        Layout.preferredWidth: 1068
                        value: 1.0
                    }
                }














            }

            Item {
            }








        }
    }







}
















































