################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/adc.c \
../Src/communication.c \
../Src/config.c \
../Src/dma.c \
../Src/encoders.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/line_sensor.c \
../Src/main.c \
../Src/motors.c \
../Src/pid.c \
../Src/state_machine.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_hal_timebase_tim.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c \
../Src/task_encoder_handler.c \
../Src/task_line_sensor_handler.c \
../Src/task_manager.c \
../Src/task_motor_controller.c \
../Src/task_normal_run.c \
../Src/task_track_recording.c \
../Src/tim.c \
../Src/usart.c 

OBJS += \
./Src/adc.o \
./Src/communication.o \
./Src/config.o \
./Src/dma.o \
./Src/encoders.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/line_sensor.o \
./Src/main.o \
./Src/motors.o \
./Src/pid.o \
./Src/state_machine.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_hal_timebase_tim.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o \
./Src/task_encoder_handler.o \
./Src/task_line_sensor_handler.o \
./Src/task_manager.o \
./Src/task_motor_controller.o \
./Src/task_normal_run.o \
./Src/task_track_recording.o \
./Src/tim.o \
./Src/usart.o 

C_DEPS += \
./Src/adc.d \
./Src/communication.d \
./Src/config.d \
./Src/dma.d \
./Src/encoders.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/line_sensor.d \
./Src/main.d \
./Src/motors.d \
./Src/pid.d \
./Src/state_machine.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_hal_timebase_tim.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d \
./Src/task_encoder_handler.d \
./Src/task_line_sensor_handler.d \
./Src/task_manager.d \
./Src/task_motor_controller.d \
./Src/task_normal_run.d \
./Src/task_track_recording.d \
./Src/tim.d \
./Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F401xE -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Inc" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Users/kamil/OneDrive/Pulpit/STM32_WS/LF_STM32_FreeRTOS/Drivers/CMSIS/Include"  -O3 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


